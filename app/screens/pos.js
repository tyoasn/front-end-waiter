import React, {Component} from "react";
import { View, StyleSheet, Image, ScrollView, TouchableOpacity } from 'react-native';
import { StatusBar } from "react-native";
import { Container, Drawer, Header, Title, Left, Right, Button, Body, Content,Text, Card, CardItem, Form, Item, Input, Label, } from "native-base";
import { Icon, SearchBar } from 'react-native-elements'
import {Col, Row, Grid} from 'react-native-easy-grid';
import { Dropdown } from 'react-native-material-dropdown';
import styles from '../assets/styles/styles';
import cup from '../assets/img/cup.png';
import DrawerPos from '../drawer/sidemenu';
import ModalAddNewCust from '../modal/modalnewcust';
import ModalEditNewCust from '../modal/modaleditcust';
import ModalTable from '../modal/modaltable';
import ModalOrderDetail from '../modal/modalorderdetail';
import ModalYesno from '../modal/modalyesno';
import ModalPayment from '../modal/modalpayment';
import Dialog, { DialogTitle, DialogContent, SlideAnimation, } from 'react-native-popup-dialog';
 

export default class Pos extends Component {

state = {
    modaladdnewcustomer: false,
    modaltable: false,
    modaleditcustomer: false,
    modalitem: false,
    modalyesno: false,
    modalpayment: false
  };


  closeDrawer() {
      this.drawer._root.close()
    };
  openDrawer() {
      this.drawer._root.open()
    };


  constructor(props) {
    console.disableYellowBox = true;
    super(props);
    this.state = {
      selected2: undefined,
    };
  }
  onValueChange2(value: string) {
    this.setState({
      selected2: value
    });
  }
  render() {

    let dataProduk = [{
      value: 'Produk 1',
    }, {
      value: 'Produk 2',
    }, {
      value: 'Produk 3',
    }];

    let dataSort = [{
      value: 'A-Z',
    }, {
      value: 'Harga',
    }];


    return (
      <Drawer
        ref={(ref) => { this.drawer = ref; }}
        content={<DrawerPos navigator={this._navigator} />}
        onClose={() => this.closeDrawer()}
        panCloseMask = {0.8}
        openDrawerOffset = {0.8}
        >
 
      <Container>
        <Header style={styles.Header}>
          <Left style={{flex:1}}>
          <Button
            transparent
            onPress={() => this.props.navigation.openDrawer()}>
            <Icon name="menu" color='#ab3737'/>
          </Button>
          </Left>
          <Body style={{flex:1}}>
          </Body>
          <Right style={{flex:1}}>
          </Right>
        </Header>
        <StatusBar
          backgroundColor="#fff"
          barStyle="light-content"
        />
        <Content padder scrollEnabled={false} style={styles.contentPage}>
        <Grid>
        <Col size={65} style={styles.container70}>
          <View style={styles.searchInput}>
            <SearchBar
              round
              lightTheme
              clearIcon={{ color: 'red' }}
              containerStyle={styles.inputSearch}
              inputStyle={styles.inputSearch2}
              searchIcon={{ size: 24 }}
              onChangeText={null}
              onClear={null}
              placeholder='Pencarian Produk'
            />
            <Button style={styles.btnRed} onPress={() => {this.setState({modaltable: true,});}}>
              <Text style={styles.txtWhite}>Table Management</Text>
            </Button>
            </View>
            <View style={styles.searchInput}>
            <Dropdown
              label='Semua Produk'
              data={dataProduk}
              inputContainerStyle={styles.inputPicker}
            />
            <Dropdown
              label='Urutkan Berdasarkan'
              data={dataSort}
              inputContainerStyle={styles.inputPicker}
            />
          </View>
            <ScrollView style={styles.scrollProduct}>
            <View style={styles.itemContainer}>
            
              <Card style={styles.itemProduct}>
              <TouchableOpacity onPress={() => {this.setState({modalitem: true,});}}>
                <CardItem cardBody>
                    <Image source={require('../assets/img/nasi-goreng.jpg')} style={styles.imageProduct}/>
                  </CardItem>
                  <CardItem>
                    <Text style={styles.txtProduct}>
                      Nasi Goreng Kambing
                    </Text>
                  </CardItem>
                  </TouchableOpacity>
                </Card>
              
              <Card style={styles.itemProduct}>
              <TouchableOpacity onPress={() => {this.setState({modalitem: true,});}}>
                <CardItem cardBody>
                    <Image source={require('../assets/img/nasi-goreng.jpg')} style={styles.imageProduct}/>
                  </CardItem>
                  <CardItem>
                    <Text style={styles.txtProduct}>
                      Nasi Goreng Kambing
                    </Text>
                  </CardItem>
                  </TouchableOpacity>
                </Card>

              <Card style={styles.itemProduct}>
              <TouchableOpacity onPress={() => {this.setState({modalitem: true,});}}>
                <CardItem cardBody>
                    <Image source={require('../assets/img/nasi-goreng.jpg')} style={styles.imageProduct}/>
                  </CardItem>
                  <CardItem>
                    <Text style={styles.txtProduct}>
                      Nasi Goreng Kambing
                    </Text>
                  </CardItem>
                  </TouchableOpacity>
                </Card>

              <Card style={styles.itemProduct}>
              <TouchableOpacity onPress={() => {this.setState({modalitem: true,});}}>
                <CardItem cardBody>
                    <Image source={require('../assets/img/nasi-goreng.jpg')} style={styles.imageProduct}/>
                  </CardItem>
                  <CardItem>
                    <Text style={styles.txtProduct}>
                      Nasi Goreng Kambing
                    </Text>
                  </CardItem>
                  </TouchableOpacity>
                </Card>

                </View>
              </ScrollView>
            </Col>
            <Col size={35} style={styles.container30}>
              <View style={styles.headerContainer30}>
                <Text style={styles.headingContainer30}>Daftar Order</Text>
                <View style={styles.infoContainer1}>
                  <View style={styles.leftBtn}>
                    <Text style={styles.dateOrder}>Thu, 15 November 2018</Text>
                  </View>
                  <View style={styles.rightBtn}>
                    <Text style={styles.infoServer2}>#001212</Text>
                  </View>
                </View>
                <View style={styles.infoContainer2}>
                  <View style={styles.leftBtn2}>
                    <Button rounded style={styles.btnWhite} onPress={() => {this.setState({modaladdnewcustomer: true,
                    });}}>
                      <Text style={styles.btnText}>Add Customer</Text>
                    </Button>
                    <Button rounded style={styles.btnWhite} onPress={() => {this.setState({modaleditcustomer: true,
                    });}}>
                      <Text style={styles.btnText}>Edit</Text>
                    </Button>
                  </View>
                  <View style={styles.rightBtn2}>
                    <Text style={styles.infoServer}>Server</Text>
                    <Text style={styles.infoServer2}>Denny K</Text>
                  </View>
                </View>
              </View>
              <ScrollView style={styles.scrollOrderDetail3}>

                  <View style={styles.orderItem}>
                  <View style={styles.orderNumber}>
                    <Text>1</Text>
                  </View>
                  <View style={styles.orderList}>
                  <View style={styles.listCont30}>
                    <Text style={styles.orderTitleList2}>Tacos with Soy, Lime, and Spice</Text>
                    <Text style={styles.priceOrderList}>Rp. 15.350.000,-</Text>
                  </View>
                    <Text style={styles.orderVariantList}>Tidak Pedas</Text>
                  </View>
                  </View>

                 <View style={styles.orderItem}>
                  <View style={styles.orderNumber}>
                    <Text>1</Text>
                  </View>
                  <View style={styles.orderList}>
                  <View style={styles.listCont30}>
                    <Text style={styles.orderTitleList2}>Tacos with Soy, Lime, and Spice</Text>
                    <Text style={styles.priceOrderList}>Rp. 50.000,-</Text>
                  </View>
                    <Text style={styles.orderVariantList}>Tidak Pedas</Text>
                  </View>
                  </View>

                  <View style={styles.orderItem}>
                  <View style={styles.orderNumber}>
                    <Text>1</Text>
                  </View>
                  <View style={styles.orderList}>
                  <View style={styles.listCont30}>
                    <Text style={styles.orderTitleList2}>Tacos with Soy, Lime, and Spice</Text>
                    <Text style={styles.priceOrderList}>Rp. 50.000,-</Text>
                  </View>
                    <Text style={styles.orderVariantList}>Tidak Pedas</Text>
                  </View>
                  </View>

                  <View style={styles.orderItem}>
                  <View style={styles.orderNumber}>
                    <Text>1</Text>
                  </View>
                  <View style={styles.orderList}>
                  <View style={styles.listCont30}>
                    <Text style={styles.orderTitleList2}>Tacos with Soy, Lime, and Spice</Text>
                    <Text style={styles.priceOrderList}>Rp. 50.000,-</Text>
                  </View>
                    <Text style={styles.orderVariantList}>Tidak Pedas</Text>
                  </View>
                  </View>

                  <View style={styles.orderItem}>
                  <View style={styles.orderNumber}>
                    <Text>1</Text>
                  </View>
                  <View style={styles.orderList}>
                  <View style={styles.listCont30}>
                    <Text style={styles.orderTitleList2}>Tacos with Soy, Lime, and Spice</Text>
                    <Text style={styles.priceOrderList}>Rp. 50.000,-</Text>
                  </View>
                    <Text style={styles.orderVariantList}>Tidak Pedas</Text>
                  </View>
                  </View>

                  <View style={styles.orderItem}>
                  <View style={styles.orderNumber}>
                    <Text>1</Text>
                  </View>
                  <View style={styles.orderList}>
                  <View style={styles.listCont30}>
                    <Text style={styles.orderTitleList2}>Tacos with Soy, Lime, and Spice</Text>
                    <Text style={styles.priceOrderList}>Rp. 50.000,-</Text>
                  </View>
                    <Text style={styles.orderVariantList}>Tidak Pedas</Text>
                  </View>
                  </View>

                  <View style={styles.orderItem}>
                  <View style={styles.orderNumber}>
                    <Text>1</Text>
                  </View>
                  <View style={styles.orderList}>
                  <View style={styles.listCont30}>
                    <Text style={styles.orderTitleList2}>Tacos with Soy, Lime, and Spice</Text>
                    <Text style={styles.priceOrderList}>Rp. 50.000,-</Text>
                  </View>
                    <Text style={styles.orderVariantList}>Tidak Pedas</Text>
                  </View>
                  </View>

                  <View style={styles.orderItem}>
                  <View style={styles.orderNumber}>
                    <Text>1</Text>
                  </View>
                  <View style={styles.orderList}>
                  <View style={styles.listCont30}>
                    <Text style={styles.orderTitleList2}>Tacos with Soy, Lime, and Spice</Text>
                    <Text style={styles.priceOrderList}>Rp. 50.000,-</Text>
                  </View>
                    <Text style={styles.orderVariantList}>Tidak Pedas</Text>
                  </View>
                  </View>

                  <View style={styles.orderItem}>
                  <View style={styles.orderNumber}>
                    <Text>1</Text>
                  </View>
                  <View style={styles.orderList}>
                  <View style={styles.listCont30}>
                    <Text style={styles.orderTitleList2}>Tacos with Soy, Lime, and Spice</Text>
                    <Text style={styles.priceOrderList}>Rp. 50.000,-</Text>
                  </View>
                    <Text style={styles.orderVariantList}>Tidak Pedas</Text>
                  </View>
                  </View>

              </ScrollView>
              <View style={styles.btnContainerBottom}>
              <View>
                  <View style={styles.listCont302}>
                    <Text style={styles.orderTitleList3}>Subtotal</Text>
                    <Text style={styles.priceOrderList}>Rp. 15.500.000,-</Text>
                  </View>
                  <View style={styles.listCont302}>
                    <Text style={styles.orderTitleList3}>Discount</Text>
                    <Text style={styles.priceOrderList}>-</Text>
                  </View>
                  <View style={styles.listCont302}>
                    <Text style={styles.orderTitleList3}>Service Tax</Text>
                    <Text style={styles.priceOrderList}>Rp. 10.600,-</Text>
                  </View>
                  <View style={styles.listCont302}>
                    <Text style={styles.orderTitleList4}>TOTAL</Text>
                    <Text style={styles.priceOrderList2}>Rp. 17.116.600,-</Text>
                  </View>

              </View>
              <View style={styles.containerBtn}>
                <View style={styles.btnLeft3}>
                <Button
                  transparent onPress={() => {this.setState({modalyesno: true,});}}>
                    <Icon name="delete" type="material" size={30} color='#ab3737'/>
                </Button>
                </View>
                <View style={styles.btnLeft3}>
                <Button
                  transparent>
                    <Icon name="print" type="material" size={30} color='#ab3737'/>
                </Button>
                </View>
                <View style={styles.btnLeft3}>
                <Button rounded style={styles.btnPayOrder} onPress={() => {this.setState({modalyesno: true,});}}>
                  <Text style={styles.txtBtnWhite2}>Simpan</Text>
                </Button>
                </View>
                <View style={styles.btnLeft3}>
                <Button rounded style={styles.btnPayOrder} onPress={() => {this.setState({modalpayment: true,});}}>
                  <Text style={styles.txtBtnWhite2}>Bayar</Text>
                </Button>
                </View>
              </View>
              </View>
            </Col>
          </Grid>

      <Dialog
            visible={this.state.modaladdnewcustomer}
            dialogAnimation={new SlideAnimation({
            slideFrom: 'bottom',
            })}
            onTouchOutside={() => {
            this.setState({ modaladdnewcustomer: false });
          }}>
            <DialogContent>
              {
                <View style={styles.modalAddNewCust}>
                <Container style={styles.modalContainer}>
                  <Header style={styles.headerDialog}>
                    <Left style={{flex:1}}>
                      <Text style={styles.titleModal}>Add New Customer</Text>
                    </Left>
                    <Body style={{flex:1}}>
                    </Body>
                    <Right style={{flex:1}}>
                    <Button
                      transparent
                      onPress={() => {this.setState({ modaladdnewcustomer: false });}}>
                      <Icon name="close" color='#fff'/>
                    </Button>
                    </Right>
                  </Header>
                    <ModalAddNewCust/>
                </Container>
                </View>
            }
          </DialogContent>
        </Dialog>

        <Dialog
            visible={this.state.modaleditcustomer}
            dialogAnimation={new SlideAnimation({
            slideFrom: 'bottom',
            })}
            onTouchOutside={() => {
            this.setState({ modaleditcustomer: false });
          }}>
            <DialogContent>
              {
                <View style={styles.modalAddNewCust}>
                <Container style={styles.modalContainer}>
                  <Header style={styles.headerDialog}>
                    <Left style={{flex:1}}>
                      <Text style={styles.titleModal}>Edit New Customer</Text>
                    </Left>
                    <Body style={{flex:1}}>
                    </Body>
                    <Right style={{flex:1}}>
                    <Button
                      transparent
                      onPress={() => {this.setState({ modaleditcustomer: false });}}>
                      <Icon name="close" color='#fff'/>
                    </Button>
                    </Right>
                  </Header>
                    <ModalAddNewCust/>
                </Container>
                </View>
            }
          </DialogContent>
        </Dialog>


        <Dialog
            visible={this.state.modaltable}
            dialogAnimation={new SlideAnimation({
            slideFrom: 'bottom',
            })}
            onTouchOutside={() => {
            this.setState({ modaltable: false });
          }}>
            
            <DialogContent>
              {
                <View style={styles.modalTableManagement}>
                <Container style={styles.modalContainer}>
                  <Header style={styles.headerDialog}>
                    <Left style={{flex:1}}>
                      <Text style={styles.titleModal}>Table Management</Text>
                    </Left>
                    <Body style={{flex:1}}>
                    </Body>
                    <Right style={{flex:1}}>
                    <Button
                      transparent
                      onPress={() => {this.setState({ modaltable: false });}}>
                      <Icon name="close" color='#fff'/>
                    </Button>
                    </Right>
                  </Header>
                    <ModalTable/>
              </Container>
            </View>
            }
          </DialogContent>
        </Dialog>

        <Dialog
            visible={this.state.modalitem}
            dialogAnimation={new SlideAnimation({
            slideFrom: 'bottom',
            })}
            onTouchOutside={() => {
            this.setState({ modalitem: false });
          }}>
            
            <DialogContent>
              {
              <View style={styles.modalOrderDetail}>
                <Container style={styles.modalContainer}>
                  <Header style={styles.headerDialog}> 
                    <Left style={{flex:1}}>
                      <Text style={styles.titleModal}>Order Detail</Text>
                    </Left>
                    <Body style={{flex:1}}>
                    </Body>
                    <Right style={{flex:1}}>
                    <Button
                      transparent
                      onPress={() => {this.setState({ modalitem: false });}}>
                      <Icon name="close" color='#fff'
                    />
                    </Button>
                    </Right>
                  </Header>
                    <ModalOrderDetail/>
              </Container>
            </View>
            }
          </DialogContent>
        </Dialog>

        <Dialog
            visible={this.state.modalyesno}
            dialogAnimation={new SlideAnimation({
            slideFrom: 'bottom',
            })}
            onTouchOutside={() => {
            this.setState({ modalyesno: false });
          }}>
            
            <DialogContent>
              {
              <View style={styles.modalAddNewCust3}>
                <Container style={styles.modalContainer}>
                  <Header style={styles.headerDialog}> 
                    <Left style={{flex:1}}>
                      <Text style={styles.titleModal}>Yes / No</Text>
                    </Left>
                    <Body style={{flex:1}}>
                    </Body>
                    <Right style={{flex:1}}>
                    <Button
                      transparent
                      onPress={() => {this.setState({ modalyesno: false });}}>
                      <Icon name="close" color='#fff'
                    />
                    </Button>
                    </Right>
                  </Header>
                    <ModalYesno/>
              </Container>
            </View>
            }
          </DialogContent>
        </Dialog>

        <Dialog
            visible={this.state.modalpayment}
            dialogAnimation={new SlideAnimation({
              slideFrom: 'bottom',
            })}
            onTouchOutside={() => {
            this.setState({ modalpayment: false });
          }}
            >
            <DialogContent>
              {
                <View style={styles.modalAddNewCust2}>
                <Container style={styles.modalContainer}>
                  <Header style={styles.headerDialog}>
                    <Left style={{flex:1}}>
                      <Text style={styles.titleModal}>Payment Method</Text>
                    </Left>
                    <Body style={{flex:1}}>
                    </Body>
                    <Right style={{flex:1}}>
                    <Button
                      transparent
                      onPress={() => {this.setState({ modalpayment: false });}}>
                      <Icon name="close" color='#fff'/>
                    </Button>
                    </Right>
                  </Header>
                    <ModalPayment/>
              </Container>
            </View>
            }
          </DialogContent>
        </Dialog>

        </Content>
      </Container>
    </Drawer>
    );
  }
}