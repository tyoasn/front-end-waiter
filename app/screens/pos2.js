import React, {Component} from "react";
import { View, StyleSheet, Image, ScrollView } from 'react-native';
import { StatusBar } from "react-native";
import { Container, Drawer, Header, Title, Left, Right, Button, Body, Content,Text, Card, CardItem, Form, Item, Input, Label, } from "native-base";
import Dialog, { DialogTitle, DialogContent, SlideAnimation, } from 'react-native-popup-dialog';
import { Icon, SearchBar } from 'react-native-elements'
import {Col, Row, Grid} from 'react-native-easy-grid';
import { Dropdown } from 'react-native-material-dropdown';
import styles from '../assets/styles/styles';
import cup from '../assets/img/cup.png';
import DrawerPos from './sidemenu';


export default class Pos extends Component {

  closeDrawer() {
      this.drawer._root.close()
    };
  openDrawer() {
      this.drawer._root.open()
    };

  constructor(props) {
    console.disableYellowBox = true;
    super(props);
    this.state = {
      selected2: undefined,
    };
  }
  onValueChange2(value: string) {
    this.setState({
      selected2: value
    });
  }
  render() {

    let dataProduk = [{
      value: 'Produk 1',
    }, {
      value: 'Produk 2',
    }, {
      value: 'Produk 3',
    }];

    let dataSort = [{
      value: 'A-Z',
    }, {
      value: 'Harga',
    }];

    let dataLantai = [{
      value: 'Lantai 1',
    }, {
      value: 'Lantai 2',
    }];

    return (
      <Drawer
        ref={(ref) => { this.drawer = ref; }}
        content={<DrawerPos navigator={this._navigator} />}
        onClose={() => this.closeDrawer()}
        panCloseMask = {0.8}
        >
 
      <Container>
        <Header style={styles.Header}>
          <Left style={{flex:1}}>
          <Button
            transparent
            onPress={() => this.openDrawer()}>
            <Icon name="menu" color='#ab3737'/>
          </Button>
          </Left>
          <Body style={{flex:1}}>
          </Body>
          <Right style={{flex:1}}>
          </Right>
        </Header>
        <StatusBar
          backgroundColor="#fff"
          barStyle="light-content"
        />
        <Content padder scrollEnabled={false} style={styles.contentPage}>
        <Grid>
        <Col size={65} style={styles.container70}>
          <View style={styles.searchInput}>
            <SearchBar
              round
              lightTheme
              clearIcon={{ color: 'red' }}
              containerStyle={styles.inputSearch}
              inputStyle={styles.inputSearch2}
              searchIcon={{ size: 24 }}
              onChangeText={null}
              onClear={null}
              placeholder='Pencarian Produk'
            />
            <Button style={styles.btnRed}>
              <Text style={styles.txtWhite}>Table Management</Text>
            </Button>
            </View>
            <View style={styles.searchInput}>
            <Dropdown
              label='Semua Produk'
              data={dataProduk}
              inputContainerStyle={styles.inputPicker}
            />
            <Dropdown
              label='Urutkan Berdasarkan'
              data={dataSort}
              inputContainerStyle={styles.inputPicker}
            />
          </View>
            <ScrollView style={styles.scrollProduct}>
            <View style={styles.itemContainer}>
              <Card style={styles.itemProduct}>
                <CardItem cardBody>
                    <Image source={require('../assets/img/nasi-goreng.jpg')} style={styles.imageProduct}/>
                  </CardItem>
                  <CardItem>
                    <Text style={styles.txtProduct}>
                      Nasi Goreng Kambing
                    </Text>
                  </CardItem>
                </Card>
                <Card style={styles.itemProduct}>
                  <CardItem cardBody>
                    <Image source={require('../assets/img/nasi-goreng.jpg')} style={styles.imageProduct}/>
                  </CardItem>
                  <CardItem>
                    <Text style={styles.txtProduct}>
                      Nasi Goreng Kambing
                    </Text>
                  </CardItem>
                </Card> 
                <Card style={styles.itemProduct}>
                  <CardItem cardBody>
                    <Image source={require('../assets/img/nasi-goreng.jpg')} style={styles.imageProduct}/>
                  </CardItem>
                  <CardItem>
                    <Text style={styles.txtProduct}>
                      Nasi Goreng Kambing
                    </Text>
                  </CardItem>
                </Card>
                <Card style={styles.itemProduct}>
                  <CardItem cardBody>
                    <Image source={require('../assets/img/nasi-goreng.jpg')} style={styles.imageProduct}/>
                  </CardItem>
                  <CardItem>
                    <Text style={styles.txtProduct}>
                      Nasi Goreng Kambing
                    </Text>
                  </CardItem>
                </Card> 
                </View>
              </ScrollView>
            </Col>

            <Col size={35} style={styles.container30}>
              <View style={styles.headerContainer30}>
                <Text style={styles.headingContainer30}>Daftar Order</Text>
                <View style={styles.infoContainer1}>
                  <View style={styles.leftBtn}>
                    <Text style={styles.dateOrder}>Thu, 15 November 2018</Text>
                  </View>
                  <View style={styles.rightBtn}>
                    <Text style={styles.infoServer2}>#001212</Text>
                  </View>
                </View>
                <View style={styles.infoContainer2}>
                  <View style={styles.leftBtn2}>
                    <Button rounded style={styles.btnWhite} onPress={() => {this.setState({ visible: true });}}>
                      <Text style={styles.btnText}>Add Customer</Text>
                    </Button>
                    <Button rounded style={styles.btnWhite}>
                      <Text style={styles.btnText}>Edit</Text>
                    </Button>
                  </View>
                  <View style={styles.rightBtn2}>
                    <Text style={styles.infoServer}>Server</Text>
                    <Text style={styles.infoServer2}>Denny K</Text>
                  </View>
                </View>
              </View>
              <View style={styles.cupContainer}>
                <Image source={cup} style={styles.cup}/>
                <Text style={styles.cupEmpty}>Belum ada pesanan</Text>
              </View>
            </Col>

            
          </Grid>
        </Content>
      </Container>
    </Drawer>
    );
  }
}