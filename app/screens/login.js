import React, {Component} from "react";
import {Platform} from 'react-native';
import {StatusBar} from "react-native";
import {
  View,
  ImageBackground,
  Image,
  TextInput,
  Dimensions,
  TouchableOpacity,
  AsyncStorage,
  Alert
} from 'react-native';
import { Icon } from 'react-native-elements'
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Text,InputGroup, Input } from 'native-base';
import styles from '../assets/styles/styles'
import logo from '../assets/img/logo.png'

export default class Login extends Component {

  constructor(props) {
    console.disableYellowBox = true;
    super(props);
  }

    render() {
    return (

       <Container style={styles.loginContainer}>
        <Header noShadow style={styles.headerLogin}>
          <Left style={{flex:1}}>
          </Left>
          <Body style={{flex:1}}>    
          </Body>
          <Right style={{flex:1}}>
          </Right>
        </Header>
        <StatusBar backgroundColor="#222" barStyle="light-content"/>
        <Content>
          <View style={styles.logoContainer}>
            <Image source={logo} style={styles.logo}/>
          </View>
            <View>
            <InputGroup style={styles.input}>
              <Icon name='user' type="evilicon" size={30} color='#ab3737'/>
              <Input placeholder='Username'/>
            </InputGroup>
            <InputGroup style={styles.input}>
              <Icon name='lock' type="evilicon" size={30} color='#ab3737'/>
              <Input placeholder='Password' secureTextEntry={true}/>
            </InputGroup>
            <Button rounded style={styles.btnLogin} onPress={()=> this.props.navigation.navigate('DrawerNavigation')}>
              <Text style={styles.loginText}>Login</Text>
            </Button>
            </View>
        </Content>
      </Container>
    );
  }
}

