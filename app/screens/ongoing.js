import React, {Component} from "react";
import { View, StyleSheet, Image, ScrollView, TouchableOpacity } from 'react-native';
import { StatusBar } from "react-native";
import { Container, Drawer, Header, Title, Left, Right, Button, Body, Content,Text, Card, CardItem, Form, Item, Input, Label, ListItem, CheckBox, Tab, Tabs } from "native-base";
import Dialog, { DialogTitle, DialogContent, SlideAnimation, } from 'react-native-popup-dialog';
import { Icon, SearchBar } from 'react-native-elements'
import {Col, Row, Grid} from 'react-native-easy-grid';
import { Dropdown } from 'react-native-material-dropdown';
import styles from '../assets/styles/styles';
import cup from '../assets/img/cup.png';
import DrawerPos from '../drawer/sidemenu';
import Tab1 from '../tab/tabOngoing1';
import Tab2 from '../tab/tabOngoing2';
import ModalPayment from '../modal/modalpayment';


export default class Ongoing extends Component {

  state = {
    modalpayment: false,
    modal2: false,
    modal3: false,
    modal4: false
  };


  constructor(props) {
    console.disableYellowBox = true;
    super(props);
    this.state = {
      selected2: undefined,
    };
  }
  onValueChange2(value: string) {
    this.setState({
      selected2: value
    });
  }
  render() {

    let dataProduk = [{
      value: 'Produk 1',
    }, {
      value: 'Produk 2',
    }, {
      value: 'Produk 3',
    }];

    let dataLantai = [{
      value: 'Lantai 1',
    }, {
      value: 'Lantai 2',
    }];

    return (
      <Container>
        <Header style={styles.Header}>
          <Left style={{flex:1}}>
          <Button
            transparent
            onPress={() => this.props.navigation.openDrawer()}>
            <Icon name="menu" color='#ab3737'/>
          </Button>
          </Left>
          <Body style={{flex:1}}>
          </Body>
          <Right style={{flex:1}}>
          </Right>
        </Header>
        <StatusBar
          backgroundColor="#fff"
          barStyle="light-content"
        />
        <Content padder scrollEnabled={false} style={styles.contentPage}>
        <Grid>
        <Col size={65} style={styles.container70}>
          <View style={styles.searchInput}>
            <SearchBar
              round
              lightTheme
              clearIcon={{ color: 'red' }}
              containerStyle={styles.inputSearch3}
              inputStyle={styles.inputSearch2}
              searchIcon={{ size: 24 }}
              onChangeText={null}
              onClear={null}
              placeholder='Pencarian Riwayat Order'
            />
            </View>
            <View style={styles.searchInput}>
            <Dropdown
              label='Semua Produk'
              data={dataProduk}
              inputContainerStyle={styles.inputPicker}
            />
            <ListItem style={styles.chkShow}>
              <CheckBox checked={true} />
              <Body>
                <Text style={styles.txtCheckShowTable}>Tampilkan meja yang akun ini layani</Text>
              </Body>
            </ListItem>
          </View>
            <ScrollView style={styles.scrollProduct}>
            <View style={styles.itemContainer}>
              <Card style={styles.itemList}>
              <TouchableOpacity>
                <CardItem cardBody style={styles.cardList}>
                  <View style={styles.numberList}>
                    <Text style={styles.txtNumberList}>01</Text>
                  </View>

                  <View style={styles.listCont1}>
                    <View style={styles.tableContainerList}>
                      <Text style={styles.txtTableList}>Table 1</Text>
                      <Text style={styles.txtTableCust}>Nina</Text>
                    </View>
                    <View style={styles.tableContainerList}>
                      <Text style={styles.txtTableList}>#001</Text>
                      <Text style={styles.txtTableCust}>1923</Text>
                    </View>
                  </View>

                  <View style={styles.listCont1}>
                    <View style={styles.tableContainerList}>
                      <Text style={styles.txtName}>Server : <Text style={styles.txtTableList}>Denny K</Text></Text>
                      <Text style={styles.txtName}>Item : <Text style={styles.txtTableList}>4/4</Text></Text>
                    </View>
                  </View>

                  <View style={styles.listCont2}>
                    <View style={styles.tableContainerList}>
                      <View style={styles.listFinish}>
                      <Text style={styles.txtName2}>Order</Text>
                      <Icon name="cancel" type="material" size={20} color='#ab3737'/>
                      </View>
                      <View style={styles.listFinish}>
                      <Text style={styles.txtName2}>Pembayaran</Text>
                      <Icon name="check-circle" type="material" size={20} color='#79F1FF'/>
                      </View>
                    </View>
                  </View>
                  </CardItem>
                  </TouchableOpacity>
                </Card>
                <Card style={styles.itemList}>
                <TouchableOpacity>
                <CardItem cardBody style={styles.cardList2}>
                  <View style={styles.numberList2}>
                    <Text style={styles.txtNumberList}>02</Text>
                  </View>
                  <View style={styles.listCont3}>
                    <View style={styles.tableContainerList}>
                      <Text style={styles.txtTableList2}>Table 1</Text>
                      <Text style={styles.txtTableCust2}>Nina</Text>
                    </View>
                    <View style={styles.tableContainerList}>
                      <Text style={styles.txtTableList2}>#001</Text>
                      <Text style={styles.txtTableCust2}>1923</Text>
                    </View>
                  </View>
                  <View style={styles.listCont3}>
                    <View style={styles.tableContainerList}>
                      <Text style={styles.txtName3}>Server : <Text style={styles.txtTableList2}>Denny K</Text></Text>
                      <Text style={styles.txtName3}>Item : <Text style={styles.txtTableList2}>4/4</Text></Text>
                    </View>
                  </View>
                  <View style={styles.listCont4}>
                    <View style={styles.tableContainerList}>
                      <View style={styles.listFinish}>
                      <Text style={styles.txtName3}>Order</Text>
                      <Icon name="cancel" type="material" size={20} color='#fff'/>
                      </View>
                      <View style={styles.listFinish}>
                      <Text style={styles.txtName3}>Pembayaran</Text>
                      <Icon name="check-circle" type="material" size={20} color='#79F1FF'/>
                      </View>
                    </View>
                  </View>
                  </CardItem>
                  </TouchableOpacity>
                </Card>

                <Card style={styles.itemList}>
                <TouchableOpacity>
                <CardItem cardBody style={styles.cardList}>
                  <View style={styles.numberList}>
                    <Text style={styles.txtNumberList}>01</Text>
                  </View>

                  <View style={styles.listCont1}>
                    <View style={styles.tableContainerList}>
                      <Text style={styles.txtTableList}>Table 1</Text>
                      <Text style={styles.txtTableCust}>Nina</Text>
                    </View>
                    <View style={styles.tableContainerList}>
                      <Text style={styles.txtTableList}>#001</Text>
                      <Text style={styles.txtTableCust}>1923</Text>
                    </View>
                  </View>

                  <View style={styles.listCont1}>
                    <View style={styles.tableContainerList}>
                      <Text style={styles.txtName}>Server : <Text style={styles.txtTableList}>Denny K</Text></Text>
                      <Text style={styles.txtName}>Item : <Text style={styles.txtTableList}>4/4</Text></Text>
                    </View>
                  </View>

                  <View style={styles.listCont2}>
                    <View style={styles.tableContainerList}>
                      <View style={styles.listFinish}>
                      <Text style={styles.txtName2}>Order</Text>
                      <Icon name="cancel" type="material" size={20} color='#ab3737'/>
                      </View>
                      <View style={styles.listFinish}>
                      <Text style={styles.txtName2}>Pembayaran</Text>
                      <Icon name="check-circle" type="material" size={20} color='#79F1FF'/>
                      </View>
                    </View>
                  </View>

                  </CardItem>
                  </TouchableOpacity>
                </Card>

                </View>
              </ScrollView>
            </Col>
            <Col size={35} style={styles.container30}>
              <View style={styles.headerContainer30}>
                <Text style={styles.headingContainer30}>Daftar Order</Text>
                <View style={styles.infoContainer1}>
                  <View style={styles.leftBtn}>
                    <Text style={styles.dateOrder}>Thu, 15 November 2018</Text>
                  </View>
                  <View style={styles.rightBtn}>
                    <Text style={styles.infoServer2}>#001212</Text>
                  </View>
                </View>
                <View style={styles.infoContainer2}>
                  <View style={styles.leftBtn2}>
                  <View style={styles.listTableHistory}>
                    <Text style={styles.tableListOrder}>Table 3</Text>
                    <Text style={styles.tableListName}>Serena</Text>
                  </View>
                  </View>
                  <View style={styles.rightBtn2}>
                    <Text style={styles.infoServer}>Server</Text>
                    <Text style={styles.infoServer2}>Denny K</Text>
                  </View>
                </View>
              </View>
              <Tabs tabBarUnderlineStyle={{backgroundColor: 'transparent'}}>
                <Tab heading="Transaksi" textStyle={{color: '#fff'}} tabStyle={{backgroundColor: '#999'}} activeTabStyle={{backgroundColor: '#fff'}} activeTextStyle={{color: '#ab3737'}}>
                  <Tab1/>
                </Tab>
                <Tab heading="Order" textStyle={{color: '#fff'}} tabStyle={{backgroundColor: '#999'}} activeTabStyle={{backgroundColor: '#fff'}} activeTextStyle={{color: '#ab3737'}}>
                  <Tab2/>
                </Tab>
              </Tabs>

              <View style={styles.btnContainerBottom}>
              <View>
                  <View style={styles.listCont302}>
                    <Text style={styles.orderTitleList3}>Subtotal</Text>
                    <Text style={styles.priceOrderList}>Rp. 15.500.000,-</Text>
                  </View>
                  <View style={styles.listCont302}>
                    <Text style={styles.orderTitleList3}>Discount</Text>
                    <Text style={styles.priceOrderList}>-</Text>
                  </View>
                  <View style={styles.listCont302}>
                    <Text style={styles.orderTitleList3}>Service Tax</Text>
                    <Text style={styles.priceOrderList}>Rp. 10.600,-</Text>
                  </View>
                  <View style={styles.listCont302}>
                    <Text style={styles.orderTitleList4}>TOTAL</Text>
                    <Text style={styles.priceOrderList2}>Rp. 17.116.600,-</Text>
                  </View>
              </View>
              <View style={styles.containerBtn}>
                <View style={styles.btnLeft3}>
                <Button
                  transparent>
                    <Icon name="delete" type="material" size={30} color='#ab3737'/>
                </Button>
                </View>
                <View style={styles.btnLeft3}>
                <Button
                  transparent>
                    <Icon name="print" type="material" size={30} color='#ab3737'/>
                </Button>
                </View>
                <View style={styles.btnLeft3}>
                <Button rounded style={styles.btnPayOrder} onPress={() => {this.setState({modalpayment: true,});}}>
                  <Text style={styles.txtBtnWhite2}>Bayar</Text>
                </Button>
                </View>
                <View style={styles.btnLeft3}>
                <Button rounded style={styles.btnPayOrder}>
                  <Text style={styles.txtBtnWhite2}>Void</Text>
                </Button>
                </View>
              </View>
              </View>
            </Col>
          </Grid>

          <Dialog
            visible={this.state.modalpayment}
            dialogAnimation={new SlideAnimation({
              slideFrom: 'bottom',
            })}
            onTouchOutside={() => {
            this.setState({ modalpayment: false });
          }}
            >
            <DialogContent>
              {
                <View style={styles.modalAddNewCust2}>
                <Container style={styles.modalContainer}>
                  <Header style={styles.headerDialog}>
                    <Left style={{flex:1}}>
                      <Text style={styles.titleModal}>Payment Method</Text>
                    </Left>
                    <Body style={{flex:1}}>
                    </Body>
                    <Right style={{flex:1}}>
                    <Button
                      transparent
                      onPress={() => {this.setState({ modalpayment: false });}}>
                      <Icon name="close" color='#fff'/>
                    </Button>
                    </Right>
                  </Header>
                    <ModalPayment/>
              </Container>
            </View>
            }
          </DialogContent>
        </Dialog>
        </Content>
      </Container>
    );
  }
}