import React, {Component} from "react";
import { View, StyleSheet, Image, ScrollView, List, TouchableOpacity,} from 'react-native';
import { StatusBar } from "react-native";
import { Container, Drawer, Header, Title, Left, Right,  Body, Content,Text, Card, CardItem, Form, Item, Input, Label, Thumbnail } from "native-base";
import Dialog, { DialogTitle, DialogContent, SlideAnimation, } from 'react-native-popup-dialog';
import { Icon, SearchBar } from 'react-native-elements';
import {Col, Row, Grid} from 'react-native-easy-grid';
import { Dropdown } from 'react-native-material-dropdown';
import styles from '../assets/styles/styles';



 export default class DrawerPos extends Component {

    constructor(){
      super();
      this.state ={
        status:false
      }
    }
 
    ShowHideTextComponentView = () =>{
      if(this.state.status == true)
      {
        this.setState({status: false})
      }
      else
      {
        this.setState({status: true})
      }
    }

  render() {



    let dataMenu = [{
      value: 'Ongoing Transaction',
    }, {
      value: 'Finished Transaction',
    }];

    return (
      <Container>
        <Content>
          <Header style={styles.headerDrawer}>
          <Body style={styles.centerThumb}>
            <Thumbnail large source={require('../assets/img/satset-white.jpg')} style={styles.thumbDrawer}/>
            <Text style={styles.titleThumb}>Super Restaurant</Text>
          </Body>
        </Header>
        <Header style={styles.headerDrawer2}>
            <Thumbnail source={require('../assets/img/ss4.png')} style={styles.thumbDrawer2}/>
            <View style={styles.nameServer}>
              <Text style={styles.titleThumb2}>John</Text>
              <Text style={styles.titleThumb3}>Server</Text>
            </View>
        </Header>
          <View style={styles.menuContainer}>
            <TouchableOpacity style={styles.listMenu} onPress={()=> this.props.navigation.navigate('Pos')}>
              <Icon name="laptop" type="material" size={30} color='#ab3737'/>
              <Text style={styles.titleMenu}>POS</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.listMenu} onPress={this.ShowHideTextComponentView}>
              <Icon name="history" type="material" size={30} color='#ab3737'/>
              <Text style={styles.titleMenu}>RIWAYAT</Text>
            </TouchableOpacity>
            <View>
            {
              this.state.status ?
              <View>
              <TouchableOpacity style={styles.listMenu} onPress={()=> this.props.navigation.navigate('Ongoing')}>
                <Text style={styles.titleMenu}>Ongoing Transaction</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.listMenu} onPress={()=> this.props.navigation.navigate('Finished')}>
                <Text style={styles.titleMenu}>Finished Transaction</Text>
              </TouchableOpacity>
              </View>
              :null
            }
            </View>
            <TouchableOpacity style={styles.listMenu}>
              <Icon name="live-help" type="material" size={30} color='#ab3737'/>
              <Text style={styles.titleMenu}>TUTORIAL</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.listMenu}>
              <Icon name="call" type="material" size={30} color='#ab3737'/>
              <Text style={styles.titleMenu}>HUBUNGI KAMI</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.listMenu}>
              <Icon name="account-circle" type="material" size={30} color='#ab3737'/>
              <Text style={styles.titleMenu}>AKUN</Text>
            </TouchableOpacity>
          </View>
        </Content>
      </Container>
    );
  }
}



            