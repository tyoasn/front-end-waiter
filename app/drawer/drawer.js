import React, { Component } from "react";
import Pos from '../screens/pos';
import Ongoing from '../screens/ongoing';
import Finished from '../screens/finished';
import DrawerPos from './sidemenu';
import { DrawerNavigator } from "react-navigation";

const DrawerNavigation = DrawerNavigator(
  {
   	Pos: { screen: Pos }, 
    Ongoing: { screen: Ongoing },
    Finished: { screen: Finished }
  },
{

contentComponent: props => <DrawerPos {...props} />,

}
);
export default DrawerNavigation;