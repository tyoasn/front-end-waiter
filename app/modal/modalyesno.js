import React, {Component} from "react";
import { View, StyleSheet, Image, ScrollView } from 'react-native';
import { StatusBar } from "react-native";
import { Container, Header, Title, Left, Right, Button, Body, Content,Text, Card, CardItem, Form, Item, Input, Label, } from "native-base";
import Dialog, { DialogTitle, DialogContent, SlideAnimation, } from 'react-native-popup-dialog';
import { Icon, SearchBar } from 'react-native-elements'
import {Col, Row, Grid} from 'react-native-easy-grid';
import { Dropdown } from 'react-native-material-dropdown';
import styles from '../assets/styles/styles'

 export default class ModalYesno extends Component {
    constructor(props) {
    console.disableYellowBox = true;
    super(props);

  }
  render() {
    return (
        <Content scrollEnabled={false}>   
            <View style={styles.chkContainer3}>
              <Text style={styles.titleDelete}>Anda Yakin?</Text>
            </View>
            <View style={styles.saveBtnContainer4}>
                <Button rounded style={styles.btnSales4}>
                  <Text style={styles.txtChk3}>Ya</Text>
                </Button>
                <Button rounded style={styles.btnSales4}>
                  <Text style={styles.txtChk3}>Tidak</Text>
                </Button>
            </View>
        </Content>
    );
  }
}           




            