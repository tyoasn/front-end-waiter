import React, {Component} from "react";
import { View, StyleSheet, Image, ScrollView } from 'react-native';
import { StatusBar } from "react-native";
import { Container, Header, Title, Left, Right, Button, Body, Content,Text, Card, CardItem, Form, Item, Input, Label, } from "native-base";
import Dialog, { DialogTitle, DialogContent, SlideAnimation, } from 'react-native-popup-dialog';
import { Icon, SearchBar } from 'react-native-elements'
import {Col, Row, Grid} from 'react-native-easy-grid';
import { Dropdown } from 'react-native-material-dropdown';
import styles from '../assets/styles/styles'

 export default class ModalPayment extends Component {
    constructor(props) {
    console.disableYellowBox = true;
    super(props);

  }
  render() {
    return (
                  <Content>
                      <View style={styles.chkContainer2}>
                        <Text style={styles.titleSales}>Total Payment</Text>
                      </View>
                      <View style={styles.chkContainer2}>
                        <Text style={styles.pricePay}>Rp. 116.000,-</Text>
                      </View>                  
                      <View style={styles.chkContainer2}>
                        <Text style={styles.titleSales}>Payment Method</Text>
                      </View>
                      <View style={styles.chkContainer}>
                        <View style={styles.chkSales}>
                        <Button rounded style={styles.btnSales}>
                          <Text style={styles.txtChk}>Cash</Text>
                        </Button>
                        </View>

                      <View style={styles.chkSales}>
                        <Button rounded style={styles.btnSales2}>
                          <Text style={styles.txtChk2}>Debit/Card</Text>
                        </Button>
                        </View>
                        <View style={styles.chkSales}>
                        <Button rounded style={styles.btnSales2}>
                          <Text style={styles.txtChk2}>Doku</Text>
                        </Button>
                        </View>
                      </View>
                      <View style={styles.saveBtnContainer3}>
                          <Button rounded style={styles.btnSales3}>
                            <Text style={styles.txtChk3}>Bayar</Text>
                          </Button>
                      </View>
                  </Content>
    );
  }
}           




            