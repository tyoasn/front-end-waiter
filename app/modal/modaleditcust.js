import React, {Component} from "react";
import { View, StyleSheet, Image, ScrollView } from 'react-native';
import { StatusBar } from "react-native";
import { Container, Header, Title, Left, Right, Button, Body, Content,Text, Card, CardItem, Form, Item, Input, Label, } from "native-base";
import { Icon, SearchBar } from 'react-native-elements'
import {Col, Row, Grid} from 'react-native-easy-grid';
import { Dropdown } from 'react-native-material-dropdown';
import styles from '../assets/styles/styles';

 export default class ModalEditNewCust extends Component {
    constructor(props) {
    console.disableYellowBox = true;
    super(props);

  }
  render() {
    return (
                  <Content>
                    <Form style={styles.formAddCst}>
                      <Item stackedLabel style={styles.fieldCustom}>
                        <Label style={styles.labelForm}>Nama</Label>
                        <Input style={styles.inputForm} />
                      </Item>
                      <Item stackedLabel style={styles.fieldCustom}>
                        <Label style={styles.labelForm}>Email</Label>
                        <Input style={styles.inputForm} />
                      </Item>
                      <Item stackedLabel style={styles.fieldCustom}>
                        <Label style={styles.labelForm}>No Telepon</Label>
                        <Input style={styles.inputForm} />
                      </Item>
                      <View style={styles.chkContainer2}>
                        <Text style={styles.titleSales}>Sales Type</Text>
                      </View>
                      <View style={styles.chkContainer}>
                        <View style={styles.chkSales}>
                        <Button rounded style={styles.btnSales}>
                          <Text style={styles.txtChk}>Dine In</Text>
                        </Button>
                        </View>
                        <View style={styles.chkSales}>
                        <Button rounded style={styles.btnSales2}>
                          <Text style={styles.txtChk2}>Takeaway</Text>
                        </Button>
                        </View>
                      </View>
                      <View style={styles.chkContainer2}>
                        <Text style={styles.titleSales}>Table</Text>
                      </View>
                      <View style={styles.chkContainer}>
                        <View style={styles.chkSales}>
                        <Button rounded style={styles.btnSales2}>
                          <Text style={styles.txtChk2}>Table Management</Text>
                        </Button>
                        </View>
                      </View>
                      <View style={styles.saveBtnContainer}>
                          <Button rounded style={styles.btnSales3}>
                            <Text style={styles.txtChk3}>Simpan</Text>
                          </Button>
                      </View>
                    </Form>
                  </Content>
                  
    );
  }
}           




            