import React, {Component} from "react";
import { View, StyleSheet, Image, ScrollView, TouchableOpacity } from 'react-native';
import { StatusBar } from "react-native";
import { Container, Header, Title, Left, Right, Button, Body, Content,Text, Card, CardItem, Form, Item, Input, Label, } from "native-base";
import { Icon, SearchBar } from 'react-native-elements'
import {Col, Row, Grid} from 'react-native-easy-grid';
import { Dropdown } from 'react-native-material-dropdown';
import styles from '../assets/styles/styles'

 export default class ModalTable extends Component {
  render() {

    let dataLantai = [{
      value: 'Lantai 1',
    }, {
      value: 'Lantai 2',
    }];

    return (

              
                  <Content scrollEnabled={false}>
                  <View style={styles.searchInput}>
                    <Dropdown
                      label='Pilih Lantai'
                      data={dataLantai}
                      inputContainerStyle={styles.inputLantai}
                    />
                  </View>
                  <ScrollView style={styles.scrollTable}>
            <View style={styles.tableContainer}>
              <Card style={styles.itemTable}>
              <TouchableOpacity>
                  <CardItem style={styles.tableDetail}>
                    <Text style={styles.txtTable1}>
                      Table 1
                    </Text>
                    <Text style={styles.txtTable3}>
                      John
                    </Text>
                    <Text style={styles.txtTable2}>
                      2 pax
                    </Text>
                  </CardItem>
                </TouchableOpacity>
                </Card>
              <Card style={styles.itemTable2}>
                <TouchableOpacity>
                  <CardItem style={styles.tableDetail2}>
                    <Text style={styles.txtTable1a}>
                      Table 2
                    </Text>
                    <Text style={styles.txtTable3a}>
                      John
                    </Text>
                    <Text style={styles.txtTable2a}>
                      5 pax
                    </Text>
                  </CardItem>
                  </TouchableOpacity>
                </Card>
              <Card style={styles.itemTable3}>
                <TouchableOpacity>
                  <CardItem style={styles.tableDetail3}>
                    <Text style={styles.txtTable1a}>
                      Table 3
                    </Text>
                    <Text style={styles.txtTable3a}>
                      John
                    </Text>
                    <Text style={styles.txtTable2a}>
                      5 pax
                    </Text>
                  </CardItem>
                  </TouchableOpacity>
                </Card>
              <Card style={styles.itemTable}>
                <TouchableOpacity>
                  <CardItem style={styles.tableDetail}>
                    <Text style={styles.txtTable1}>
                      Table 4
                    </Text>
                    <Text style={styles.txtTable3}>
                      John
                    </Text>
                    <Text style={styles.txtTable2}>
                      2 pax
                    </Text>
                  </CardItem>
                  </TouchableOpacity>
                </Card>
              <Card style={styles.itemTable}>
                <TouchableOpacity>
                  <CardItem style={styles.tableDetail}>
                    <Text style={styles.txtTable1}>
                      Table 5
                    </Text>
                    <Text style={styles.txtTable3}>
                      John
                    </Text>
                    <Text style={styles.txtTable2}>
                      2 pax
                    </Text>
                  </CardItem>
                  </TouchableOpacity>
                </Card>
                <Card style={styles.itemTable}>
                <TouchableOpacity>
                  <CardItem style={styles.tableDetail}>
                    <Text style={styles.txtTable1}>
                      Table 6
                    </Text>
                    <Text style={styles.txtTable3}>
                      John
                    </Text>
                    <Text style={styles.txtTable2}>
                      2 pax
                    </Text>
                  </CardItem>
                  </TouchableOpacity>
                </Card>
                <Card style={styles.itemTable}>
                <TouchableOpacity>
                  <CardItem style={styles.tableDetail}>
                    <Text style={styles.txtTable1}>
                      Table 7
                    </Text>
                    <Text style={styles.txtTable3}>
                      John
                    </Text>
                    <Text style={styles.txtTable2}>
                      2 pax
                    </Text>
                  </CardItem>
                  </TouchableOpacity>
                </Card>
                <Card style={styles.itemTable}>
                <TouchableOpacity>
                  <CardItem style={styles.tableDetail}>
                    <Text style={styles.txtTable1}>
                      Table 8
                    </Text>
                    <Text style={styles.txtTable3}>
                      John
                    </Text>
                    <Text style={styles.txtTable2}>
                      2 pax
                    </Text>
                  </CardItem>
                  </TouchableOpacity>
                </Card>
                <Card style={styles.itemTable}>
                <TouchableOpacity>
                  <CardItem style={styles.tableDetail}>
                    <Text style={styles.txtTable1}>
                      Table 9
                    </Text>
                    <Text style={styles.txtTable3}>
                      John
                    </Text>
                    <Text style={styles.txtTable2}>
                      2 pax
                    </Text>
                  </CardItem>
                  </TouchableOpacity>
                </Card>
                <Card style={styles.itemTable}>
                <TouchableOpacity>
                  <CardItem style={styles.tableDetail}>
                    <Text style={styles.txtTable1}>
                      Table 10
                    </Text>
                    <Text style={styles.txtTable3}>
                      John
                    </Text>
                    <Text style={styles.txtTable2}>
                      2 pax
                    </Text>
                  </CardItem>
                  </TouchableOpacity>
                </Card>
                <Card style={styles.itemTable}>
                <TouchableOpacity>
                  <CardItem style={styles.tableDetail}>
                    <Text style={styles.txtTable1}>
                      Table 11
                    </Text>
                    <Text style={styles.txtTable3}>
                      John
                    </Text>
                    <Text style={styles.txtTable2}>
                      2 pax
                    </Text>
                  </CardItem>
                  </TouchableOpacity>
                </Card>
                <Card style={styles.itemTable}>
                <TouchableOpacity>
                  <CardItem style={styles.tableDetail}>
                    <Text style={styles.txtTable1}>
                      Table 12
                    </Text>
                    <Text style={styles.txtTable3}>
                      John
                    </Text>
                    <Text style={styles.txtTable2}>
                      2 pax
                    </Text>
                  </CardItem>
                  </TouchableOpacity>
                </Card>
                <Card style={styles.itemTable}>
                <TouchableOpacity>
                  <CardItem style={styles.tableDetail}>
                    <Text style={styles.txtTable1}>
                      Table 13
                    </Text>
                    <Text style={styles.txtTable3}>
                      John
                    </Text>
                    <Text style={styles.txtTable2}>
                      2 pax
                    </Text>
                  </CardItem>
                  </TouchableOpacity>
                </Card>
                <Card style={styles.itemTable}>
                <TouchableOpacity>
                  <CardItem style={styles.tableDetail}>
                    <Text style={styles.txtTable1}>
                      Table 14
                    </Text>
                    <Text style={styles.txtTable3}>
                      John
                    </Text>
                    <Text style={styles.txtTable2}>
                      2 pax
                    </Text>
                  </CardItem>
                  </TouchableOpacity>
                </Card>
                <Card style={styles.itemTable}>
                <TouchableOpacity>
                  <CardItem style={styles.tableDetail}>
                    <Text style={styles.txtTable1}>
                      Table 15
                    </Text>
                    <Text style={styles.txtTable3}>
                      John
                    </Text>
                    <Text style={styles.txtTable2}>
                      2 pax
                    </Text>
                  </CardItem>
                  </TouchableOpacity>
                </Card>
                </View>
                </ScrollView>
                <View style={styles.saveBtnContainer2}>
                  <Button rounded style={styles.btnSales3}>
                    <Text style={styles.txtChk3}>Simpan</Text>
                  </Button>
                </View>
                </Content>
                
      
    );
  }
}           




            