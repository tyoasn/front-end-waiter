import React, {Component} from "react";
import { View, StyleSheet, Image, ScrollView } from 'react-native';
import { StatusBar } from "react-native";
import { Container, Header, Title, Left, Right, Button, Body, Content,Text, Card, CardItem, Form, Item, Input, Label, } from "native-base";
import Dialog, { DialogTitle, DialogContent, SlideAnimation, } from 'react-native-popup-dialog';
import { Icon, SearchBar } from 'react-native-elements'
import {Col, Row, Grid} from 'react-native-easy-grid';
import { Dropdown } from 'react-native-material-dropdown';
import styles from '../assets/styles/styles'

 export default class ModalOrderDetail extends Component {
    constructor(props) {
    console.disableYellowBox = true;
    super(props);

  }
  render() {
    return (


                <Content scrollEnabled={false}>
                <Grid>
                  <Col size={65} style={styles.container702}>
                  <Form>
                  <View style={styles.titleOrderContainer}>
                  <Image source={require('../assets/img/nasi-goreng.jpg')} style={styles.imageProductOrder}/>
                  <View style={styles.titleOrderContainer2}>
                  <Text style={styles.titleOrder}>
                    Tuna Tacos with Soy, Lime, and Spice
                  </Text>
                  <Text style={styles.priceOrder}>
                    Rp. 54.000,-
                  </Text>
                  </View>
                  </View>
                  <ScrollView style={styles.scrollOrderDetail}>
                  <View style={styles.titleVariantContainer}>
                    <Text style={styles.titleVariant}>
                      Variant
                    </Text>
                    <View style={styles.btnContainerVariant}>
                    <View style={styles.containerBtnVariant}>
                    <View style={styles.btnLeft3}>
                    <Button rounded style={styles.btnVariant}>
                      <Text style={styles.txtBtnVariant}>Variant 1</Text>
                    </Button>
                    </View>
                    <View style={styles.btnLeft3}>
                    <Button rounded style={styles.btnVariant1}>
                      <Text style={styles.txtBtnVariant1}>Variant 2</Text>
                    </Button>
                    </View>
                    <View style={styles.btnLeft3}>
                    <Button rounded style={styles.btnVariant1}>
                      <Text style={styles.txtBtnVariant1}>Variant 3</Text>
                    </Button>
                    </View>
                    </View>
                  </View>
                  </View>
                  <View style={styles.titleVariantContainer}>
                    <Text style={styles.titleVariant}>
                      Modifier
                    </Text>
                    <View style={styles.btnContainerVariant}>
                    <View style={styles.containerBtnVariant}>
                    <View style={styles.btnLeft3}>
                    <Button rounded style={styles.btnVariant}>
                      <Text style={styles.txtBtnVariant}>Modifier 1</Text>
                    </Button>
                    </View>
                    <View style={styles.btnLeft3}>
                    <Button rounded style={styles.btnVariant1}>
                      <Text style={styles.txtBtnVariant1}>Modifier 2</Text>
                    </Button>
                    </View>
                    <View style={styles.btnLeft3}>
                    <Button rounded style={styles.btnVariant1}>
                      <Text style={styles.txtBtnVariant1}>Modifier 3</Text>
                    </Button>
                    </View>
                    </View>
                  </View>
                  </View>
                  </ScrollView>
                  <View style={styles.containerBottomOrder}>
                      <Item stackedLabel style={styles.fieldCustom}>
                        <Label style={styles.titleVariant}>Notes</Label>
                        <Input style={styles.inputForm} />
                      </Item>
                      <View style={styles.saveBtnContainer}>
                        <Button
                          transparent>
                          <Icon name="plus" type="evilicon" size={40} color='#ab3737'/>
                        </Button>
                      </View>
                      </View>
                  </Form>
                  </Col>
                  <Col size={35} style={styles.container30}>
                  <View style={styles.cont30modal}>
                  <View style={styles.titleOrderContainer}>
                    <Text style={styles.titleOrder}>
                      Orders
                    </Text>
                  </View>
                  <ScrollView style={styles.scrollOrderDetail2}>
                  <View style={styles.orderItem}>
                  <View style={styles.orderNumber}>
                    <Text>1</Text>
                  </View>
                  <View style={styles.orderList}>
                    <Text style={styles.orderTitleList}>Tacos with Soy, Lime, and Spice</Text>
                    <Text style={styles.orderVariantList}>Tidak Pedas</Text>
                  </View>
                  </View>
                  <View style={styles.orderItem}>
                  <View style={styles.orderNumber}>
                    <Text>2</Text>
                  </View>
                  <View style={styles.orderList}>
                    <Text style={styles.orderTitleList}>Tacos with Soy, Lime, and Spice</Text>
                  </View>
                  </View>
                  </ScrollView>
                  </View>
                  <View style={styles.btnContainerBottom}>
                  <View style={styles.containerBtn}>
                    <View style={styles.btnLeft3}>
                    <Button rounded style={styles.btnPayOrder}>
                      <Text style={styles.txtBtnWhite2}>Tambah</Text>
                    </Button>
                    </View>
                  </View>
                  </View>
                  </Col>
                </Grid>
                </Content>
    );
  }
}           




            