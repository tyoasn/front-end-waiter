import React, {Component} from "react";
import { View, StyleSheet, Image, ScrollView, } from 'react-native';
import { StatusBar } from "react-native";
import { Container, Drawer, Header, Title, Left, Right, Button, Body, Content,Text, Card, CardItem, Form, Item, Input, Label, ListItem, CheckBox, Tab, Tabs } from "native-base";
import Dialog, { DialogTitle, DialogContent, SlideAnimation, } from 'react-native-popup-dialog';
import { Icon, SearchBar } from 'react-native-elements'
import {Col, Row, Grid} from 'react-native-easy-grid';
import { Dropdown } from 'react-native-material-dropdown';
import styles from '../assets/styles/styles';
import cup from '../assets/img/cup.png';
import { Divider } from 'react-native-elements'




export default class Tab2 extends Component {

  render() {
    return (
            <View style={styles.containerTab1}>
              <ScrollView style={styles.scrollOrderDetail4}>

                <View>
                  <View style={styles.orderTabHeading}>
                    <Text style={styles.txtOrderTabHeading}>Menu Utama</Text>
                  </View>
                  <View style={styles.contentOrderTab}>
                  <Text style={styles.titleOrderMenu}>Nasi Goreng Kambing</Text>
                  <Text>Modifier : 3 Emping</Text>
                  <View style={styles.containerIconOrder}>
                    <View>
                      <Text style={styles.lineOrderTab}>________________________________</Text>
                    </View>
                    <View style={styles.iconBlock}>
                      <Icon name="check-box" type="material" size={30} color='#ab3737'/>
                      <Text style={styles.txtIconBlock}>Masuk Antrian</Text>
                    </View>
                    <View style={styles.iconBlock}>
                      <Icon name="check-box" type="material" size={30} color='#ab3737'/>
                      <Text style={styles.txtIconBlock}>Proses Masak</Text>
                    </View>
                    <View style={styles.iconBlock}>
                      <Icon name="check-box" type="material" size={30} color='#ab3737'/>
                      <Text style={styles.txtIconBlock}>Siap Diantar</Text>
                    </View>
                    <View style={styles.iconBlock}>
                      <Icon name="check-box" type="material" size={30} color='#555'/>
                      <Text style={styles.txtIconBlock}>Sudah Diantar</Text>
                    </View>
                    <View style={styles.iconBlock}>
                      <Icon name="room-service" type="material" size={40} color='#ffb84d'/>
                    </View>
                  </View>
                  </View>
                  <View style={styles.contentOrderTab}>
                  <Text style={styles.titleOrderMenu}>Cheese Burger</Text>
                  <Text>Modifier : 3 Emping</Text>
                  <View style={styles.containerIconOrder}>
                    <View>
                      <Text style={styles.lineOrderTab}>________________________________</Text>
                    </View>
                    <View style={styles.iconBlock}>
                      <Icon name="check-box" type="material" size={30} color='#ab3737'/>
                      <Text style={styles.txtIconBlock}>Masuk Antrian</Text>
                    </View>
                    <View style={styles.iconBlock}>
                      <Icon name="check-box" type="material" size={30} color='#ab3737'/>
                      <Text style={styles.txtIconBlock}>Proses Masak</Text>
                    </View>
                    <View style={styles.iconBlock}>
                      <Icon name="check-box" type="material" size={30} color='#555'/>
                      <Text style={styles.txtIconBlock}>Siap Diantar</Text>
                    </View>
                    <View style={styles.iconBlock}>
                      <Icon name="check-box" type="material" size={30} color='#555'/>
                      <Text style={styles.txtIconBlock}>Sudah Diantar</Text>
                    </View>
                    <View style={styles.iconBlock}>
                      <Icon name="restaurant" type="material" size={40} color='#00e699'/>
                    </View>
                  </View>
                  </View>
                  <View style={styles.contentOrderTab}>
                  <Text style={styles.titleOrderMenu}>Cheese Burger</Text>
                  <Text>Modifier : 3 Emping</Text>
                  <View style={styles.containerIconOrder}>
                    <View>
                      <Text style={styles.lineOrderTab}>________________________________</Text>
                    </View>
                    <View style={styles.iconBlock}>
                      <Icon name="check-box" type="material" size={30} color='#ab3737'/>
                      <Text style={styles.txtIconBlock}>Masuk Antrian</Text>
                    </View>
                    <View style={styles.iconBlock}>
                      <Icon name="check-box" type="material" size={30} color='#555'/>
                      <Text style={styles.txtIconBlock}>Proses Masak</Text>
                    </View>
                    <View style={styles.iconBlock}>
                      <Icon name="check-box" type="material" size={30} color='#555'/>
                      <Text style={styles.txtIconBlock}>Siap Diantar</Text>
                    </View>
                    <View style={styles.iconBlock}>
                      <Icon name="check-box" type="material" size={30} color='#555'/>
                      <Text style={styles.txtIconBlock}>Sudah Diantar</Text>
                    </View>
                    <View style={styles.iconBlock}>
                    <Button transparent style={styles.btnCancelOrder}>
                      <Icon name="cancel" type="material" size={40} color='#ab3737'/>
                      </Button>
                    </View>
                  </View>
                  </View>
                </View>

                <View>
                  <View style={styles.orderTabHeading}>
                    <Text style={styles.txtOrderTabHeading}>Minuman</Text>
                  </View>
                  <View style={styles.contentOrderTab}>
                  <Text style={styles.titleOrderMenu}>Es Lemon Teh</Text>
                  <View style={styles.containerIconOrder}>
                    <View>
                      <Text style={styles.lineOrderTab}>________________________________</Text>
                    </View>
                    <View style={styles.iconBlock}>
                      <Icon name="check-box" type="material" size={30} color='#ab3737'/>
                      <Text style={styles.txtIconBlock}>Masuk Antrian</Text>
                    </View>
                    <View style={styles.iconBlock}>
                      <Icon name="check-box" type="material" size={30} color='#ab3737'/>
                      <Text style={styles.txtIconBlock}>Proses Masak</Text>
                    </View>
                    <View style={styles.iconBlock}>
                      <Icon name="check-box" type="material" size={30} color='#ab3737'/>
                      <Text style={styles.txtIconBlock}>Siap Diantar</Text>
                    </View>
                    <View style={styles.iconBlock}>
                      <Icon name="check-box" type="material" size={30} color='#ab3737'/>
                      <Text style={styles.txtIconBlock}>Sudah Diantar</Text>
                    </View>
                    <View style={styles.iconBlock}>
                      <Icon name="check-circle" type="material" size={40} color='#66ffff'/>
                    </View>
                  </View>
                  </View>
                </View>


              </ScrollView>
            </View>

    );
  }
}