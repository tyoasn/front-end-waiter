import React, {Component} from "react";
import { View, StyleSheet, Image, ScrollView, } from 'react-native';
import { StatusBar } from "react-native";
import { Container, Drawer, Header, Title, Left, Right, Button, Body, Content,Text, Card, CardItem, Form, Item, Input, Label, ListItem, CheckBox, Tab, Tabs } from "native-base";
import Dialog, { DialogTitle, DialogContent, SlideAnimation, } from 'react-native-popup-dialog';
import { Icon, SearchBar } from 'react-native-elements'
import {Col, Row, Grid} from 'react-native-easy-grid';
import { Dropdown } from 'react-native-material-dropdown';
import styles from '../assets/styles/styles';
import cup from '../assets/img/cup.png';




export default class Tab1 extends Component {

  render() {
    return (
            <View style={styles.containerTab1}>
              <ScrollView style={styles.scrollOrderDetail3}>
                  <View style={styles.orderItem}>
                    <View style={styles.orderNumber}>
                      <Text>1</Text>
                    </View>
                    <View style={styles.orderList}>
                    <View style={styles.listCont30}>
                      <Text style={styles.orderTitleList2}>Tacos with Soy, Lime, and Spice</Text>
                      <Text style={styles.priceOrderList}>Rp. 15.350.000,-</Text>
                    </View>
                    <View style={styles.listCont30}>
                      <Text style={styles.orderVariantList}>+ Emping</Text>
                      <Text style={styles.priceOrderList}>+ Rp. 5.000,-</Text>
                    </View>
                    </View>
                  </View>
                  <View style={styles.orderItem}>
                    <View style={styles.orderNumber}>
                      <Text>1</Text>
                    </View>
                    <View style={styles.orderList}>
                    <View style={styles.listCont30}>
                      <Text style={styles.orderTitleList2}>Tacos with Soy, Lime, and Spice</Text>
                      <Text style={styles.priceOrderList}>Rp. 15.350.000,-</Text>
                    </View>
                    <View style={styles.listCont30}>
                      <Text style={styles.orderVariantList}>+ Emping</Text>
                      <Text style={styles.priceOrderList}>+ Rp. 5.000,-</Text>
                    </View>
                    </View>
                  </View>
                  <View style={styles.orderItem}>
                    <View style={styles.orderNumber}>
                      <Text>1</Text>
                    </View>
                    <View style={styles.orderList}>
                    <View style={styles.listCont30}>
                      <Text style={styles.orderTitleList2}>Tacos with Soy, Lime, and Spice</Text>
                      <Text style={styles.priceOrderList}>Rp. 15.350.000,-</Text>
                    </View>
                    <View style={styles.listCont30}>
                      <Text style={styles.orderVariantList}>+ Emping</Text>
                      <Text style={styles.priceOrderList}>+ Rp. 5.000,-</Text>
                    </View>
                    </View>
                  </View>
                  <View style={styles.orderItem}>
                    <View style={styles.orderNumber}>
                      <Text>1</Text>
                    </View>
                    <View style={styles.orderList}>
                    <View style={styles.listCont30}>
                      <Text style={styles.orderTitleList2}>Tacos with Soy, Lime, and Spice</Text>
                      <Text style={styles.priceOrderList}>Rp. 15.350.000,-</Text>
                    </View>
                    <View style={styles.listCont30}>
                      <Text style={styles.orderVariantList}>+ Emping</Text>
                      <Text style={styles.priceOrderList}>+ Rp. 5.000,-</Text>
                    </View>
                    </View>
                  </View>
              </ScrollView>
            </View>

    );
  }
}