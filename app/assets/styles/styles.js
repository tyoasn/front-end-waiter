import { StyleSheet } from 'react-native';

export default StyleSheet.create({

//login page 

  headerLogin : {
  backgroundColor:'transparent',
  borderWidth:0
  },
  barstatus:{
    backgroundColor:'#222'
  },

  loginContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop:120
  },
  logo: {
    width:100,
    height:150,
    resizeMode:'contain',
    flex: 1,
    justifyContent: 'center',
  },
  logoContainer:{
    alignItems:'center',
    paddingBottom:50
  },
  logoText:{
    color:'white',
    fontSize:26,
    fontWeight:'500',
    letterSpacing:1,
    marginTop:10,
    opacity:0.8
  },
  inputLogin: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
},
loginIcon : {
  marginLeft:0
},
  input:{
    flex: 1,
    width : 300,
    height:50,
    paddingLeft:0,
    borderColor: '#ab3737',
    borderBottomWidth : 1,
    paddingRight:0,
    justifyContent:'center',
    marginHorizontal:75,
    marginTop:20,
  },
  btnLogin:{
    width : 300,
    height:50,
    borderRadius:45,
    fontSize:14,
    paddingLeft:20,
    borderColor: 'transparent',
    borderWidth: 2,
    paddingRight:20,
    backgroundColor:'#ab3737',
    justifyContent:'center',
    alignItems: 'center',
    flex: 1,
    marginHorizontal:75,
    marginTop:50
  },
  loginText:{
    textAlign:'center',
    color: '#fff',
    fontSize:14,
  },
  footer: {
    backgroundColor:'#fff',
  },

// pos page

Header : {
  backgroundColor:'#fff',
},
container70 : {
  backgroundColor: '#f5f5f5',
  padding: 10,
  height : 800,
},
container702 : {
  borderRightWidth: 1,
  borderRightColor: '#f5f5f5',
},
container30 : {
  backgroundColor: '#fff',
},
itemProduct : {
  width : '24.4%',
},
imageProduct : {
  height: 100,
  width: null,
  flex: 1
},
txtProduct : {
  fontSize : 14
},
searchInput : {
  width : '100%',
  flexDirection:'row',
},
inputSearch : {
  width : '73%',
  marginRight : 10,
  borderWidth: 1,
  borderColor: 'transparent',
  backgroundColor : 'transparent',
},
inputSearch3 : {
  width : '100%',
  marginRight : 10,
  borderWidth: 1,
  borderColor: 'transparent',
  backgroundColor : 'transparent',
},
inputSearch2 : {
  borderColor: 'transparent',
  backgroundColor : '#fff'
},
btnRed : {
  backgroundColor : '#ab3737',
  borderRadius : 50,
  paddingRight : 10,
  paddingLeft : 10, 
  paddingBottom : 5,
  paddingTop : 5,
  marginTop : 5
},
txtWhite : {
  color : '#fff',
  fontSize : 14
},
inputPicker : {
  marginRight : 20,
  width : 200,
},
headerContainer30 : {
  backgroundColor : '#ab3737',
  paddingTop : 20,
  paddingLeft : 10,
  paddingRight : 10,
  paddingBottom : 20,
},
headingContainer30 : {
  color : '#fff',
  fontSize : 24,
  fontWeight : 'bold',
},
infoContainer1 : {
  flexDirection : 'row',
  marginTop : 20
},
infoContainer2 : {
  flexDirection : 'row',
  marginTop : 20,
  borderTopWidth : 2,
  paddingTop : 0,
  borderTopColor : '#fff',
},
leftBtn : {
  width : 240,
  flexDirection : 'row'
},
leftBtn2 : {
  width : 240,
  flexDirection : 'row',
  paddingTop : 20
},
leftBtn3 : {
  width : 240,
  flexDirection : 'row',
  paddingTop : 10
},
rightBtn2 : {
  borderLeftWidth : 2,
  borderLeftColor : '#fff',
  paddingTop : 20,
},

dateOrder : {
  color : '#fff',
  fontSize : 14,
  width : 250
},
numberOrder : {
  color : '#fff',
  fontSize : 16,
  textAlign : 'left',
  
},
infoServer : {
  color : '#fff',
  fontSize : 12,
  textAlign : 'right',
  paddingLeft : 20
},
infoServer2 : {
  color : '#fff',
  fontWeight : 'bold',
  fontSize : 14,
  textAlign : 'right',
  paddingLeft : 20
},
scrollProduct : {
  position : 'absolute',
  top: 150,
  left: 10,
  right: 10,
  bottom: 10,
  paddingBottom : 20,
  height : 580,
  flexDirection : 'column'
},
contentPage : {
  height : '90%',
  width : '100%',
},
itemContainer : {
  flexDirection : 'row',
  flex : 1,
  flexWrap : 'wrap'
},
btnWhite : {
  backgroundColor : '#fff',
  paddingBottom : 5,
  paddingTop : 5,
  marginRight : 45
},
btnText : {
  fontSize : 12,
  fontWeight : 'bold',
  color : '#ab3737'
},
addCstBtn : {
  width : '80%'
},
txtServer : {
  color : '#fff',
  fontSize : 12,
  textAlign : 'right'
},
  cupContainer:{
    alignItems:'center',
    paddingBottom:50
},
  cup: {
    width:200,
    height:250,
    marginTop : 50,
    resizeMode:'contain',
    flex: 1,
    justifyContent: 'center',
  },
  cupEmpty : {
    textAlign: 'center',
    fontSize : 14,
    color : '#555',
    marginTop : 20
  },
  containerBtn : {
    flexDirection : 'row',
    justifyContent: 'center',
    paddingBottom : 20,
    paddingTop: 20
  },
  btnDeleteOrder : {
    backgroundColor : '#79F1FF',
    paddingLeft : 10,
    paddingRight : 10
  },
    btnPayOrder : {
    backgroundColor : '#ab3737',
    paddingLeft : 20,
    paddingRight : 20
  },
  btnLeft3 : {
    paddingLeft : 10,
    paddingRight : 10
  },
  btnContainerBottom : {
    position : 'absolute',
    bottom : 30,
    left : 0,
    right : 0,
    paddingTop: 20,
    borderTopColor: '#ab3737',
    borderTopWidth: 1,
    backgroundColor: '#fff'
  },
  // modal add new customer

  modalAddNewCust : {
    width : 600,
    height : 550,
    backgroundColor: '#fff',
    justifyContent: 'center',
    padding : 0
  },
    modalAddNewCust2 : {
    width : 600,
    height : 350,
    backgroundColor: '#fff',
    justifyContent: 'center',
    padding : 0
  },
      modalAddNewCust3 : {
    width : 400,
    height : 250,
    backgroundColor: '#fff',
    justifyContent: 'center',
    padding : 0
  },
  headerDialog : {
    backgroundColor: '#ab3737',
    marginLeft : -20,
    marginRight : -20,
    paddingLeft : 20,
    paddingRight : 20,
    paddingBottom : 10
  },
  titleModal : {
    color : '#fff',
    fontSize : 16,
    fontWeight : 'bold'
  },
  formAddCst : {
    marginTop : 20
  },
  labelForm : {
    color : '#222',
    fontSize : 14,
    fontWeight : 'bold'
  },
  inputForm : {
    marginTop : 10,
    borderBottomWidth : 1,
    paddingLeft : 10,
    marginRight : 15,
  },
  fieldCustom : {
    paddingBottom : 0,
    borderBottomWidth : 0,
    height: 50
  },

  chkContainer : {
    flexDirection : 'row',
    marginLeft : 10,
  },
    chkContainer2 : {
    marginLeft : 13,
    marginBottom : 10,

  },
  chkSales : {
    flexDirection : 'row',
  },
  btnSales : {
    backgroundColor : '#ab3737',
    marginRight : 20
  },
  btnSales2 : {
    backgroundColor : '#fff',
    borderWidth : 1,
    borderColor : '#ab3737',
    marginRight : 20
  },
  btnSales3 : {
    backgroundColor : '#ab3737',
    paddingLeft : 40,
    paddingRight : 40,
  },
  txtChk : {
    color : '#fff'
  },
    txtChk2 : {
    color : '#ab3737'
  },
    txtChk3 : {
    color : '#fff',

  },
  titleSales : {
    color : '#222',
    fontSize : 14,
    fontWeight : 'bold',
    marginTop : 20
  },
  saveBtnContainer : {
    flexDirection : 'row',
    justifyContent: 'center',
    marginTop : 20,
  },
    saveBtnContainer3 : {
    flexDirection : 'row',
    justifyContent: 'center',
    marginTop : 40,
  },

  // modal table management

    modalTableManagement : {
    width : 900,
    height : 700,
    backgroundColor: '#fff',
    justifyContent: 'center',
    padding : 0
  },

  inputLantai : {
  marginTop : 20,
  marginRight : 20,
  width : 900,

},
tableContainer : {
  flexDirection : 'row',
  flex : 1,
  flexWrap : 'wrap',
},
scrollTable : {
  position : 'absolute',
  top: 100,
  left: 10,
  right: 10,
  bottom: 0,
  paddingBottom : 0,
  height : 400,
  flexDirection : 'column'
},
itemTable : {
  width : '18%',
  height : 120,
  marginRight : 13,
  justifyContent : 'center',
  flexDirection : 'row',
  borderRadius : 10,
  borderColor : '#222'

},
itemTable2 : {
  width : '18%',
  height : 120,
  marginRight : 13,
  justifyContent : 'center',
  flexDirection : 'row',
  borderRadius : 10,
  borderColor : '#ab3737',
  backgroundColor : '#ab3737'

},
itemTable3 : {
  width : '18%',
  height : 120,
  marginRight : 13,
  justifyContent : 'center',
  flexDirection : 'row',
  borderRadius : 10,
  borderColor : '#79F1FF',
  backgroundColor : '#79F1FF'

},
txtTable1 : {
  fontSize : 20,
  fontWeight : 'bold',
  padding : 5
},
txtTable2 : {
  fontSize : 16,
  padding : 5
},
txtTable3 : {
  fontSize : 16,
  padding : 5,
},
txtTable1a : {
  fontSize : 20,
  fontWeight : 'bold',
  padding : 5,
  color : '#fff'
},
txtTable2a : {
  fontSize : 16,
  padding : 5,
  color : '#fff'
},
txtTable3a : {
  fontSize : 16,
  padding : 5,
  color : '#fff'
},
tableDetail : {
  flexDirection : 'column',
},
tableDetail2 : {
  flexDirection : 'column',
  backgroundColor : '#ab3737'
},
tableDetail3 : {
  flexDirection : 'column',
  backgroundColor : '#79F1FF'
},
  saveBtnContainer2 : {
    flexDirection : 'row',
    justifyContent: 'center',
    marginTop: 450
  },

  // modal order detail'

    modalOrderDetail : {
    width : 900,
    height : 650,
    backgroundColor: '#fff',
    justifyContent: 'center',
    padding : 0
  },

  titleOrder : {
    fontSize : 20,
    fontWeight : 'bold',
    width: '80%',
    paddingBottom: 20
  },
  priceOrder : {
    fontWeight : 'bold',
    fontSize : 20,
  },
  titleOrderContainer : {
    flexDirection : 'row',
    paddingTop : 20,
    paddingBottom : 20
  },
    titleOrderContainer2 : {
    flexDirection : 'column',
    paddingLeft: 20

  },
  titleVariant : {
    fontSize : 20,
    fontWeight : 'bold',
    color: '#222'
  },
    btnContainerBottom2 : {
    paddingTop : 20
  },
    btnVariant : {
    backgroundColor : '#ab3737',
    paddingLeft : 30,
    paddingRight : 30
  },
    btnVariant1 : {
    backgroundColor : '#fff',
    borderColor : '#ab3737',
    borderWidth : 1,
    paddingLeft : 30,
    paddingRight : 30,
    marginBottom : 20
  },
  txtBtnVariant : {
    color : '#fff'
  },
    txtBtnVariant1 : {
    color : '#ab3737'
  },
    btnContainerVariant : {
    paddingTop : 20,
    marginBottom : 20

  },
  titleVariantContainer : {
   paddingBottom : 0,
    
  },
    containerBtnVariant : {
    flexDirection : 'row',
    justifyContent: 'center',
    flexWrap : 'wrap'
  },
  containerBottomOrder : {
    marginTop: 10,
    marginLeft: -10,
    marginBottom: 20

  },
  scrollOrderDetail : {
    height: 250
  },
    scrollOrderDetail2 : {
    height: 360
  },

  imageProductOrder : {
    width: 250,
    height: 150
  },
  cont30modal : {
    paddingLeft: 20,
    paddingRight: 20
  },
  orderItem : {
    flexDirection: 'row',
    paddingBottom: 20  
  },
  orderNumber : {
    paddingRight: 10
  },
  orderList : {
    paddingBottom: 5
  },
  orderTitleList : {
    fontWeight: 'bold',
    paddingBottom: 10
  },
    orderTitleList2 : {
    fontWeight: 'bold',
    paddingBottom: 10,
    width: '70%',

  },
      orderTitleList3 : {
    fontSize: 14,
    paddingBottom: 10,
    width: '60%'
  },
        orderTitleList4 : {
    fontSize: 18,
    fontWeight: 'bold',
    paddingBottom: 10,
    width: '60%'
  },
  orderVariantList : {
    color: '#555',
    fontSize: 12
  },

  // drawer


  headerDrawer : {
    height: 200,
    backgroundColor: '#ab3737',
    paddingBottom: 20,
  },
  headerDrawer2 : {
    height: 100,
    backgroundColor: '#fff',
    paddingBottom: 20,
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center'
  },
  thumbDrawer : {
    justifyContent: 'center',
    flexDirection : 'row',
    paddingBottom: 20,
    marginBottom: 10,
  },
  thumbDrawer2 : {
    marginRight: 10,
    marginLeft: -10,
    paddingBottom: 20,
    marginBottom: 10,
  },
  titleThumb : {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#fff',
    textAlign: 'center'  
  },
  titleThumb2 : {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#222',
    textAlign: 'left',
    paddingBottom: 5

  },
  titleThumb3 : {
    fontSize: 16,
    color: '#ab3737',
    textAlign: 'left',
    paddingBottom: 5

  },
  centerThumb : {
    width: 50,
    flex: 1
  },
  menuContainer : {
    paddingTop: 10,
    paddingBottom: 20,
   
  },
  titleMenu : {
    fontSize: 16,
    color: '#ab3737',
    paddingLeft: 10,
    marginTop: 2
  },
    titleMenuActive : {
    fontSize: 16,
    color: '#fff',
    paddingLeft: 10,
    marginTop: 2
  },
  listMenu : {
    flexDirection: 'row',
    paddingLeft: 20,
    marginBottom: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#f5f5f5',
    paddingBottom: 10
  },
    listMenuActive : {
    flexDirection: 'row',
    paddingLeft: 20,
    marginBottom: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#f5f5f5',
    paddingBottom: 10,
    paddingTop: 10,
    backgroundColor: '#ab3737'
  },
  menuHistory : {
    backgroundColor: '#ab3737',

  },

  // pos 2

    scrollOrderDetail3 : {
    padding: 20,
    height: 300,
    top: 200,
    width: '140%',
    position: 'absolute',
  },
      scrollOrderDetail4 : {
    padding: 20,
    height: 300,
    top: 200,
    width: '100%',
    position: 'absolute',
  },

  listCont30 : {
    flexDirection: 'row',
  },
    listCont302 : {
    flexDirection: 'row',
    paddingLeft: 10
  },
  priceOrderList : {
    fontSize: 12,
    fontWeight: 'bold',
    textAlign: 'right',
    flex: 1,
  },
    priceOrderList2 : {
    fontSize: 16,
    textAlign: 'right',
    flex: 1,
    fontWeight: 'bold'
  },

  // ongoing page

itemList : {
  width: '100%'
},

numberList : {
  backgroundColor: '#ab3737',
  paddingLeft: 20,
  paddingRight: 20,
  paddingTop: 30,
  height: '100%'
},
numberList2 : {
  backgroundColor: '#5e1615',
  paddingLeft: 20,
  paddingRight: 20,
  paddingTop: 30,
  height: '100%'
},
cardList2 : {
 backgroundColor: '#ab3737'
},
txtNumberList : {
  color: '#fff',
  fontWeight: 'bold' 
},
tableContainerList : {
  padding: 20
},
txtTableList : {
  fontWeight: 'bold'
},
txtTableList2 : {
  fontWeight: 'bold',
  color: '#fff'
},
txtTableCust : {
  fontSize: 16
},
txtTableCust2 : {
  fontSize: 16,
  color: '#fff'
},
listCont1 : {
  flexDirection: 'row',
  borderRightColor: '#ab3737',
  borderRightWidth: 1
},
listCont2 : {
  flexDirection: 'row',
},
listCont3 : {
  flexDirection: 'row',
  borderRightColor: '#fff',
  backgroundColor: '#ab3737',
  borderRightWidth: 1
},
listCont4 : {
  flexDirection: 'row',
  backgroundColor: '#ab3737',
},
txtName : {
  fontWeight: 'normal',
  paddingRight: 10
},
txtName2 : {
  fontWeight: 'normal',
  paddingRight: 10
},
txtName3 : {
  fontWeight: 'normal',
  color: '#fff',
  paddingRight: 10
},

listFinish : {
  flexDirection: 'row'
},
txtCheckShowTable : {
  color: '#777'
},
chkShow : {
 width: 300
},
listTableHistory : {
  flexDirection: 'column'
},
tableListOrder : {
  color: '#fff',
  fontWeight: 'bold'
},
tableListName : {
  color: '#fff'
},
// tab pages

containerTab1 : {
  marginTop: -200
},
orderTabHeading : {
  backgroundColor: '#ab3737',
  padding: 20,
},
txtOrderTabHeading : {
  color: '#fff',
  fontWeight: 'bold',
  textAlign: 'center'
},
contentOrderTab : {
  paddingTop: 20,
},
containerIconOrder : {
  flexDirection: 'row'
},
iconBlock : {
  flexDirection: 'column',
  alignItems: 'center', 
  width: 70,
  padding: 10
},
txtIconBlock : {
  fontSize: 12
},
titleOrderMenu : {
  paddingBottom: 10,
  fontWeight: 'bold'
},
lineOrderTab : {
  color: '#ab3737',
  fontWeight: 'bold',
  position: 'absolute',
  marginTop: 13,
  marginLeft: 25,
  zIndex: -5
},
btnCancelOrder : {
  height: 80,
  marginTop: -20
},
pricePay : {
  fontSize: 20,
  color: '#ab3737',
  fontWeight: 'bold',
  textDecorationLine: 'underline',
  lineHeight: 30
},
// modal yes no

chkContainer3 : {
  justifyContent: 'center',
  flex: 1,
  alignItems: 'center',
  marginTop: 20
},

titleDelete : {
  fontSize: 30,
},
  saveBtnContainer4 : {
    flexDirection : 'row',
    justifyContent: 'center',
    marginTop : 40,
  },

    btnSales4 : {
    backgroundColor : '#ab3737',
    paddingLeft : 40,
    paddingRight : 40,
    marginRight: 20
  },






});
