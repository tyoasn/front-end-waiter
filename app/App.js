import React, {Component} from "react";
import {Stylesheet, Text, View} from 'react-native';
import {createStackNavigator} from 'react-navigation';
import Login from './screens/login';
import DrawerNavigation from './drawer/drawer';

export default class App extends Component {
  render(){
    return(
      <AppStackNavigator/>
      );
  }
}

const AppStackNavigator = createStackNavigator({
  Login : Login,
  DrawerNavigation : DrawerNavigation,
},

{
headerMode:'none'
})





